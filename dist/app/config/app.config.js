/**
 * @description 应用配置
*/

/* 运行模式配置 */
(function () {
  // 开发环境配置
  const dev = {
    mode: 'dev',
    title: '开发模式',
    // 服务匹配规则列表
    rules: [
      {rule: 'host', url: '//127.0.0.1:3601'},
      {rule: 'server', url: '//127.0.0.1:3601'},
      {rule: 'proxy', url: '//127.0.0.1:3601'},
      {rule: 'pot', url: 'tigerlair'}
    ]
  }

  // 生产环境配置
  const origin = location.origin
  const prod = {
    mode: 'prod',
    title: '生产模式',
    // 服务匹配规则列表
    rules: [
      {rule: 'host', url: origin},
      {rule: 'server', url: origin},
      {rule: 'proxy', url: origin + '/tigerlair/sass/proxy'},
      {rule: 'pot', url: 'foundation'}
    ]
  }

  /* 应用配置 */
  const application = {
    dev, prod,
    // 运行模式配置
    mode: {dev, prod}.dev
  }

  window.application = window.application || {}
  window.application.tigerlair = application
})();