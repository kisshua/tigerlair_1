import Vue from 'vue'
import Router from 'vue-router'
import config from '@/micros/config'

Vue.use(Router)

// 判断环境是否是微应用打开
let microPath = ''
if (window.__POWERED_BY_QIANKUN__) {
  microPath = config.microPath
}

const router = new Router({
  routes: [
    {
      path: microPath + '/',
      name: 'root',
      component: () => import('@/views/index'),
      redirect: microPath + '/home',
      children: [
        // 首页
        {
          path: 'home',
          name: 'home',
          component: () => import('@/views/index/index')
        },
        // 导航
        {
          path: 'nave',
          name: 'nave',
          component: () => import(/* webpackChunkName: "nave" */ '@/views/nave'),
          redirect: to => {
            return microPath + '/nave/list'
          },
          children: [
            {
              path: 'list/:id?',
              name: 'nave-list',
              component: () => import(/* webpackChunkName: "nave" */ '@/views/nave/list')
            },
            {
              path: 'item/:id?',
              name: 'nave-item',
              component: () => import(/* webpackChunkName: "nave" */ '@/views/nave/item')
            },
            {
              path: 'edit/:id?',
              name: 'nave-edit',
              component: () => import(/* webpackChunkName: "nave" */ '@/views/nave/edit')
            },
            {
              path: 'rule',
              name: 'nave-rule',
              component: () => import(/* webpackChunkName: "nave" */ '@/views/nave/rule/list')
            },
            {
              path: 'envirment',
              name: 'nave-envirment',
              component: () => import(/* webpackChunkName: "nave" */ '@/views/nave/envirment/list')
            }
          ]
        },
        // 项目
        {
          path: 'project',
          name: 'project',
          component: () => import(/* webpackChunkName: "project" */ '@/views/project/index'),
          children: [
            {
              path: 'list',
              name: 'project-list',
              component: () => import(/* webpackChunkName: "project" */ '@/views/project/list')
            },
            {
              path: 'item/:id?',
              name: 'project-edit',
              component: () => import(/* webpackChunkName: "project" */ '@/views/project/edit')
            }
          ]
        },
        // 产品
        {
          path: 'product',
          name: 'product',
          component: () => import(/* webpackChunkName: "product" */ '@/views/product/index'),
          children: [
            {
              path: 'list',
              name: 'product-list',
              component: () => import(/* webpackChunkName: "product" */ '@/views/product/list')
            },
            {
              path: 'view/:id?',
              name: 'product-view',
              component: () => import(/* webpackChunkName: "product" */ '@/views/product/view')
            },
            {
              path: 'item/:id?',
              name: 'product-edit',
              component: () => import(/* webpackChunkName: "product" */ '@/views/product/edit')
            },
            {
              path: 'tree/:productId?',
              name: 'product-tree',
              component: () => import(/* webpackChunkName: "product" */ '@/views/product/tree'),
              children: [
                {
                  path: 'view/:id',
                  name: 'product-tree-view',
                  component: () => import(/* webpackChunkName: "product" */ '@/views/product/view')
                },
                {
                  path: 'page/:id',
                  name: 'product-tree-page',
                  component: () => import(/* webpackChunkName: "product" */ '@/views/page/view')
                },
                {
                  path: 'func/:id',
                  name: 'product-tree-func',
                  component: () => import(/* webpackChunkName: "product" */ '@/views/page/func/view')
                }
              ]
            }
          ]
        },
        // 页面
        {
          path: 'page',
          name: 'page',
          component: () => import(/* webpackChunkName: "page" */ '@/views/page/index'),
          children: [
            {
              path: 'list',
              name: 'page-list',
              component: () => import(/* webpackChunkName: "page" */ '@/views/page/list')
            },
            {
              path: 'item/:id?',
              name: 'page-edit',
              component: () => import(/* webpackChunkName: "page" */ '@/views/page/edit')
            },
            {
              path: 'view/:id?',
              name: 'page-view',
              component: () => import(/* webpackChunkName: "page" */ '@/views/page/view')
            },
            {
              path: 'func/list',
              name: 'page-func-list',
              component: () => import(/* webpackChunkName: "func" */ '@/views/page/func/list')
            },
            {
              path: 'func/item/:id?',
              name: 'page-func-edit',
              component: () => import(/* webpackChunkName: "func" */ '@/views/page/func/edit')
            },
            {
              path: 'func/view/:id?',
              name: 'func-view',
              component: () => import(/* webpackChunkName: "func" */ '@/views/page/func/view')
            },
            {
              path: 'tree/:pageId?',
              name: 'page-tree',
              component: () => import(/* webpackChunkName: "page" */ '@/views/page/tree'),
              children: [
                {
                  path: 'page/:id',
                  name: 'page-tree-page',
                  component: () => import(/* webpackChunkName: "page" */ '@/views/page/edit')
                },
                {
                  path: 'func/:id',
                  name: 'page-tree-func',
                  component: () => import(/* webpackChunkName: "page" */ '@/views/page/func/edit')
                }
              ]
            }
          ]
        },
        // 编码
        {
          path: 'codewell',
          name: 'codewell',
          component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/index'),
          children: [
            {
              path: 'list',
              name: 'codewell-list',
              component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/list')
            },
            {
              path: 'item/:id?',
              name: 'codewell-edit',
              component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/edit')
            },
            {
              path: 'tree/:cid?',
              name: 'codewell-tree',
              component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/tree'),
              children: [
                {
                  path: 'encode/:id',
                  name: 'codewell-tree-encode',
                  component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/encode/edit')
                }
              ]
            },
            {
              path: 'encode/list',
              name: 'encode-list',
              component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/encode/list')
            },
            {
              path: 'encode/item/:id?',
              name: 'encode-edit',
              component: () => import(/* webpackChunkName: "codewell" */ '@/views/codewell/encode/edit')
            }
          ]
        },
        // 服务
        {
          path: 'service',
          name: 'service',
          component: () => import(/* webpackChunkName: "service" */ '@/views/service/index'),
          children: [
            // 服务
            {
              path: 'inter/list',
              name: 'inter-list',
              component: () => import(/* webpackChunkName: "inter" */ '@/views/service/inter/list.vue')
            },
            {
              path: 'inter/item/:id?',
              name: 'inter-edit',
              component: () => import(/* webpackChunkName: "inter" */ '@/views/service/inter/item.vue')
            },
            {
              path: 'inter/view/:id?',
              name: 'inter-item',
              component: () => import(/* webpackChunkName: "inter" */ '@/views/service/inter/view.vue')
            },
            {
              path: 'inter/syntax',
              name: 'inter-syntax',
              component: () => import(/* webpackChunkName: "inter" */ '@/views/service/inter/syntax.vue')
            },
            // 实体
            {
              path: 'entity/list',
              name: 'entity-list',
              component: () => import(/* webpackChunkName: "entity" */ '@/views/service/entity/list.vue')
            },
            {
              path: 'entity/item/:id?',
              name: 'entity-item',
              component: () => import(/* webpackChunkName: "entity" */ '@/views/service/entity/item.vue')
            },
            // 数据库
            {
              path: 'database',
              name: 'database',
              component: () => import(/* webpackChunkName: "database" */ '@/views/database/index.vue'),
              redirect: to => {
                return microPath + '/database/list'
              },
              children: [
                {
                  path: 'list',
                  name: 'database-list',
                  component: () => import(/* webpackChunkName: "database" */ '@/views/database/list.vue')
                },
                {
                  path: 'item/:id?',
                  name: 'database-item',
                  component: () => import(/* webpackChunkName: "database" */ '@/views/database/item.vue')
                },
                {
                  path: 'edit/:id?',
                  name: 'database-edit',
                  component: () => import(/* webpackChunkName: "database" */ '@/views/database/edit.vue')
                }
              ]
            },
            // 表
            {
              path: 'table',
              name: 'table',
              component: () => import(/* webpackChunkName: "table" */ '@/views/table/index.vue'),
              redirect: to => {
                return microPath + '/table/list'
              },
              children: [
                {
                  path: 'list',
                  name: 'table-list',
                  component: () => import(/* webpackChunkName: "table" */ '@/views/table/list.vue')
                },
                {
                  path: 'item/:id?',
                  name: 'table-item',
                  component: () => import(/* webpackChunkName: "table" */ '@/views/table/item.vue')
                },
                {
                  path: 'edit/:id?',
                  name: 'table-edit',
                  component: () => import(/* webpackChunkName: "table" */ '@/views/table/edit.vue')
                }
              ]
            },
            // 分组
            {
              path: 'group',
              name: 'group',
              component: () => import(/* webpackChunkName: "group" */ '@/views/group/index.vue'),
              redirect: to => {
                return microPath + '/group/list'
              },
              children: [
                {
                  path: 'list',
                  name: 'group-list',
                  component: () => import(/* webpackChunkName: "group" */ '@/views/group/list.vue')
                },
                {
                  path: 'item/:id?',
                  name: 'group-item',
                  component: () => import(/* webpackChunkName: "group" */ '@/views/group/item.vue')
                },
                {
                  path: 'edit/:id?',
                  name: 'group-edit',
                  component: () => import(/* webpackChunkName: "group" */ '@/views/group/edit.vue')
                }
              ]
            },
          ]
        },
        // 微应用
        {
          path: 'mapp',
          name: 'mapp-index',
          component: () => import(/* webpackChunkName: "mapp" */ '@/views/mapp/index'),
          children: [
            {
              path: 'list',
              name: 'mapp-list',
              component: () => import(/* webpackChunkName: "mapp" */ '@/views/mapp/list')
            },
            {
              path: 'item/:id?',
              name: 'mapp-item',
              component: () => import(/* webpackChunkName: "mapp" */ '@/views/mapp/item')
            },
            {
              path: 'view/:id?',
              name: 'mapp-view',
              component: () => import(/* webpackChunkName: "mapp" */ '@/views/mapp/view')
            }
          ]
        },
        // 视图
        {
          path: 'dview',
          name: 'dview-index',
          component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/index'),
          children: [
            {
              path: 'page',
              name: 'dview-page',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/page/index'),
              children: [
                {
                  path: 'list',
                  name: 'dview-list',
                  component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/page/list'),
                },
                {
                  path: 'item/:id?',
                  name: 'dview-item',
                  component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/page/item')
                }
              ]
            }
          ]
        },
        // 视图导航
        {
          path: 'dview/nav',
          name: 'dview-nav',
          component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/nav/index'),
          children: [
            {
              path: 'list',
              name: 'dview-nav-list',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/nav/list')
            },
            {
              path: 'item/:id?',
              name: 'dview-nav-edit',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/nav/item')
            },
            {
              path: 'view/:id?',
              name: 'dview-nav-item',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/nav/view')
            },
            {
              path: 'tree/:navId?',
              name: 'dview-nav-tree',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/nav/tree'),
              children: [
                {
                  path: 'view/:id',
                  name: 'dview-nav-tree-view',
                  component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/nav/view')
                }
              ]
            }
          ]
        },
        // 视图组件
        {
          path: 'dview/component',
          name: 'dview-component',
          component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/component/index'),
          children: [
            {
              path: 'list',
              name: 'dview-component-list',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/component/list')
            },
            {
              path: 'item/:id?',
              name: 'dview-component-edit',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/component/edit')
            },
            {
              path: 'view/:id?',
              name: 'dview-component-view',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/component/view')
            }
          ]
        },
        // 视图注册
        {
          path: 'dview/register',
          name: 'dview-register',
          component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/register/index'),
          children: [
            {
              path: 'list',
              name: 'dview-register-list',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/register/list')
            },
            {
              path: 'item/:id?',
              name: 'dview-register-edit',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/register/edit')
            }
          ]
        },
        // 视图片段
        {
          path: 'dview/snippet',
          name: 'dview-snippet',
          component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/snippet/index'),
          children: [
            {
              path: 'list',
              name: 'dview-snippet-list',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/snippet/list')
            },
            {
              path: 'item/:id?',
              name: 'dview-snippet-edit',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/snippet/edit')
            },
            {
              path: 'view/:id?',
              name: 'dview-snippet-view',
              component: () => import(/* webpackChunkName: "dview" */ '@/views/dview/snippet/view')
            }
          ]
        },
        // 资源
        {
          path: 'resource',
          name: 'resource-index',
          component: () => import(/* webpackChunkName: "resource" */ '@/views/process/index'),
          children: [
            {
              path: 'list',
              name: 'resource-list',
              component: () => import(/* webpackChunkName: "resource" */ '@/views/resource/list'),
            },
            {
              path: 'item/:id?',
              name: 'resource-item',
              component: () => import(/* webpackChunkName: "resource" */ '@/views/resource/item')
            }
          ]
        },
        // 文件列表
        {
          path: 'arcfile',
          name: 'arcfile',
          component: () => import(/* webpackChunkName: "arcfile" */ '@/views/arcfile/index'),
          children: [
            {
              path: 'list',
              name: 'arcfile-list',
              component: () => import(/* webpackChunkName: "arcfile" */ '@/views/arcfile/list')
            },
            {
              path: 'item/:id?',
              name: 'arcfile-edit',
              component: () => import(/* webpackChunkName: "arcfile" */ '@/views/arcfile/edit')
            }
          ]
        },
        // 流程
        {
          path: 'process',
          name: 'process',
          component: () => import(/* webpackChunkName: "process" */ '@/views/process/index'),
          children: [
            {
              path: 'execute/list',
              name: 'process-execute-list',
              component: () => import(/* webpackChunkName: "process-execute" */ '@/views/process/execute/list')
            },
            {
              path: 'execute/item/:id?',
              name: 'process-execute-edit',
              component: () => import(/* webpackChunkName: "process-execute" */ '@/views/process/execute/edit')
            },
            {
              path: 'execute/design/:id',
              name: 'process-execute-design',
              component: () => import(/* webpackChunkName: "execute-design" */ '@/views/process/execute/design')
            },
            {
              path: 'execute/view/:id?',
              name: 'process-execute-view',
              component: () => import(/* webpackChunkName: "execute-view" */ '@/views/process/execute/design')
            },
            {
              path: 'exeunit/list',
              name: 'process-exeunit-list',
              component: () => import(/* webpackChunkName: "process-exeunit" */ '@/views/process/exeunit/list')
            },
            {
              path: 'exeunit/item/:id?',
              name: 'process-exeunit-edit',
              component: () => import(/* webpackChunkName: "process-exeunit" */ '@/views/process/exeunit/edit')
            },
            {
              path: 'view/list',
              name: 'process-view-list',
              component: () => import(/* webpackChunkName: "process-view" */ '@/views/process/view/list')
            },
            {
              path: 'view/item/:id?',
              name: 'process-view-edit',
              component: () => import(/* webpackChunkName: "process-view" */ '@/views/process/view/edit')
            },
            {
              path: 'view/list-defined',
              name: 'process-view-list-defined',
              component: () => import(/* webpackChunkName: "process-view" */ '@/views/process/view/list-defined')
            },
            {
              path: 'store/list',
              name: 'process-store-list',
              component: () => import(/* webpackChunkName: "store" */ '@/views/process/store/list')
            },
            {
              path: 'store/item/:id?',
              name: 'process-store-edit',
              component: () => import(/* webpackChunkName: "store" */ '@/views/process/store/edit')
            },
            {
              path: 'exegroup/list',
              name: 'process-group-list',
              component: () => import(/* webpackChunkName: "group" */ '@/views/process/exegroup/list')
            },
            {
              path: 'exegroup/item/:id?',
              name: 'process-group-edit',
              component: () => import(/* webpackChunkName: "group" */ '@/views/process/exegroup/edit')
            },
            {
              path: 'exegroup/design/:id?',
              name: 'process-group-design',
              component: () => import(/* webpackChunkName: "execute" */ '@/views/process/exegroup/design')
            },
            {
              path: 'exegroup/view/:id?',
              name: 'process-group-view',
              component: () => import(/* webpackChunkName: "execute" */ '@/views/process/exegroup/design')
            }
          ]
        },
        // 文件
        {
          path: 'dfile',
          name: 'dfile-index',
          component: () => import(/* webpackChunkName: "dfile" */ '@/views/dfile/index'),
          children: [
            {
              path: 'list',
              name: 'dfile-list',
              component: () => import(/* webpackChunkName: "dfile" */ '@/views/dfile/list'),
            },
            {
              path: 'upload',
              name: 'dfile-upload',
              component: () => import(/* webpackChunkName: "dfile" */ '@/views/dfile/upload')
            },
            {
              path: 'item/:id?',
              name: 'dfile-item',
              component: () => import(/* webpackChunkName: "dfile" */ '@/views/dfile/item')
            }
          ]
        },
        // 资源目录
        {
          path: 'pool',
          name: 'pool-index',
          component: () => import(/* webpackChunkName: "pool" */ '@/views/pool/index'),
          children: [
            {
              path: 'list',
              name: 'pool-list',
              component: () => import(/* webpackChunkName: "pool" */ '@/views/pool/list'),
            },
            {
              path: 'item/:id?',
              name: 'pool-edit',
              component: () => import(/* webpackChunkName: "pool" */ '@/views/pool/edit')
            }
          ]
        },
        // 测试
        {
          path: 'test',
          name: 'test',
          component: () => import(/* webpackChunkName: "test" */ '@/views/test/index.vue'),
          redirect: to => {
            return microPath + '/test/list'
          },
          children: [
            {
              path: 'list',
              name: 'interface-list',
              component: () => import(/* webpackChunkName: "test" */ '@/views/test/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'test-item',
              component: () => import(/* webpackChunkName: "test" */ '@/views/test/item.vue')
            },
            {
              path: 'edit/:id?',
              name: 'test-edit',
              component: () => import(/* webpackChunkName: "test" */ '@/views/test/edit.vue')
            }
          ]
        },
        // 采集
        {
          path: 'collect',
          name: 'collect',
          component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/index.vue'),
          redirect: to => {
            return microPath + '/collect/list'
          },
          children: [
            {
              path: 'inter/list',
              name: 'collect-inter',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/inter/list.vue')
            },
            {
              path: 'inter/item/:id?',
              name: 'collect-inter-item',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/inter/item.vue')
            },
            {
              path: 'inter/view/:id?',
              name: 'collect-inter-view',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/inter/item.vue')
            },
            {
              path: 'inter/group/list',
              name: 'collect-inter-group-list',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/inter-group/list.vue')
            },
            {
              path: 'inter/group/item/:id?',
              name: 'collect-inter-group-item',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/inter-group/item.vue')
            },
            {
              path: 'error/list',
              name: 'collect-error',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/error/list.vue')
            },
            {
              path: 'error/item/:id?',
              name: 'collect-error-item',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/error/item.vue')
            },
            {
              path: 'error/view/:id?',
              name: 'collect-error-view',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/error/item.vue')
            },
            {
              path: 'error/group/list',
              name: 'collect-error-group-list',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/error-group/list.vue')
            },
            {
              path: 'error/group/item/:id?',
              name: 'collect-error-group-item',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/error-group/item.vue')
            },
            {
              path: 'performance/list',
              name: 'collect-performance',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/performance/list.vue')
            },
            {
              path: 'performance/item/:id?',
              name: 'collect-performance-item',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/performance/item.vue')
            },
            {
              path: 'performance/view/:id?',
              name: 'collect-performance-view',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/performance/item.vue')
            },
            {
              path: 'performance/group/list',
              name: 'collect-performance-group-list',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/performance-group/list.vue')
            },
            {
              path: 'performance/group/item/:id?',
              name: 'collect-performance-group-item',
              component: () => import(/* webpackChunkName: "collect" */ '@/views/collect/performance-group/item.vue')
            }
          ]
        },
        // 代理
        {
          path: 'proxier',
          name: 'proxier',
          component: () => import(/* webpackChunkName: "proxier" */ '@/views/proxier/index.vue'),
          redirect: to => {
            return microPath + '/proxier/list'
          },
          children: [
            {
              path: 'list',
              name: 'proxier-list',
              component: () => import(/* webpackChunkName: "proxier" */ '@/views/proxier/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'proxier-item',
              component: () => import(/* webpackChunkName: "proxier" */ '@/views/proxier/item.vue')
            },
            {
              path: 'edit/:id?',
              name: 'proxier-edit',
              component: () => import(/* webpackChunkName: "proxier" */ '@/views/proxier/edit.vue')
            }
          ]
        },
        // 插件
        {
          path: 'plugins',
          name: 'plugins',
          component: () => import(/* webpackChunkName: "plugins" */ '@/views/plugins/index.vue'),
          redirect: to => {
            return microPath + '/plugins/list'
          },
          children: [
            {
              path: 'list',
              name: 'plugins-list',
              component: () => import(/* webpackChunkName: "plugins" */ '@/views/plugins/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'plugins-item',
              component: () => import(/* webpackChunkName: "plugins" */ '@/views/plugins/edit.vue')
            }
          ]
        },
        // 文档页面
        {
          path: 'section',
          name: 'section',
          component: () => import(/* webpackChunkName: "section" */ '@/views/section/index'),
          children: [
            {
              path: 'list',
              name: 'section-list',
              component: () => import(/* webpackChunkName: "section" */ '@/views/section/list')
            },
            {
              path: 'item/:id?',
              name: 'section-edit',
              component: () => import(/* webpackChunkName: "section" */ '@/views/section/edit')
            }
          ]
        },
        // 文档列表
        {
          path: 'archive',
          name: 'archive',
          component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/index'),
          children: [
            {
              path: 'list',
              name: 'archive-list',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/list')
            },
            {
              path: 'item/:id?',
              name: 'archive-edit',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/item')
            },
            {
              path: 'view/:id?',
              name: 'archive-view',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/view')
            },
            {
              path: 'cat/list',
              name: 'archive-cat-list',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/cat/list')
            },
            {
              path: 'cat/item/:id?',
              name: 'archive-cat-edit',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/cat/item')
            },
            {
              path: 'chapter/list',
              name: 'archive-chapter-list',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/chapter/list')
            },
            {
              path: 'chapter/item/:id?',
              name: 'archive-chapter-edit',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/chapter/item')
            },
            {
              path: 'chapter/tree/:chapterId?',
              name: 'chapter-tree',
              component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/chapter/tree'),
              children: [
                {
                  path: 'view/:id',
                  name: 'chapter-tree-view',
                  component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/chapter/view')
                },
                {
                  path: 'page/:id',
                  name: 'chapter-tree-page',
                  component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/view')
                },
                {
                  path: 'item/:id?',
                  name: 'chapter-tree-edit',
                  component: () => import(/* webpackChunkName: "archive" */ '@/views/archive/chapter/archive')
                }
              ]
            }
          ]
        },
        // 字典
        {
          path: 'dictionary',
          name: 'dictionary',
          component: () => import(/* webpackChunkName: "dictionary" */ '@/views/dictionary/index.vue'),
          redirect: to => {
            return microPath + '/dictionary/list'
          },
          children: [
            {
              path: 'list',
              name: 'dictionary-list',
              component: () => import(/* webpackChunkName: "dictionary" */ '@/views/dictionary/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'dictionary-item',
              component: () => import(/* webpackChunkName: "dictionary" */ '@/views/dictionary/item.vue')
            },
            {
              path: 'edit/:id?',
              name: 'dictionary-edit',
              component: () => import(/* webpackChunkName: "dictionary" */ '@/views/dictionary/edit.vue')
            }
          ]
        },
        // 系统
        {
          path: 'system',
          name: 'system',
          component: () => import(/* webpackChunkName: "system" */ '@/views/system/index.vue'),
          redirect: to => {
            return microPath + '/system/list'
          },
          children: [
            {
              path: 'list',
              name: 'system-list',
              component: () => import(/* webpackChunkName: "system" */ '@/views/system/list.vue')
            },
            {
              path: 'item/:id?',
              name: 'system-item',
              component: () => import(/* webpackChunkName: "system" */ '@/views/system/item.vue')
            },
            {
              path: 'edit/:id?',
              name: 'system-edit',
              component: () => import(/* webpackChunkName: "system" */ '@/views/system/edit.vue')
            }
          ]
        }
      ]
    },
    {
      path: microPath + '/dview/page/design/:id?',
      name: 'dview-design',
      component: () => import(/* webpackChunkName: "dview-design" */ '@/views/dview/page/design')
    },
    {
      path: microPath + '/dview/page/view/:id?',
      name: 'dview-view',
      component: () => import(/* webpackChunkName: "dview-view" */ '@/views/dview/page/view')
    }
  ]
})

export default router
