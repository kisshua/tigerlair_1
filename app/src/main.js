import Vue from 'vue'
import Vuex from 'vuex'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueRouter from 'vue-router'
import App from './App'
import router from './router'
import store from './store'
import { domer } from '@daelui/dogjs/dist/components.js'
import directives from '@daelui/vdog/dist/directives'
import dmap from './components/dmap'
import config from '@/micros/config'
import dog from '@daelui/dogjs/dist/index'
import $pig from '@daelui/pigjs'
import '@daelui/dogui/dist/css/boost.min.css'
import '@daelui/dogui/dist/css/theme/black.min.css'

Vue.use(ElementUI)

const isProd = process.env.NODE_ENV === 'production'
const win = typeof self !== 'undefined' ? window : global

// 初始化模块配置
let origin = location.origin
origin = isProd ? origin : '//www.daelui.cn'
$pig.init({
  registry: origin + '/pool/prod/components.json',
  root: origin + '/pool/prod/packages',
  versions: [{name: 'vue', version: '2.*.*'}]
})

/**
 * @function 获取document的body元素
*/
domer.getDocBody = function () {
  let doc = this.getDoc()
  return doc.querySelector('#micro-app') || doc.querySelector('[data-sandbox-configuration]') || doc.body
}

// 是否运行态
const ap = win.application.tigerlair
ap.mode = ap[isProd ? 'prod' : 'dev']

// 指令
directives(Vue)
// 初次加载
dmap.set('onload', 1)

Vue.config.productionTip = false

// 路径处理
let microPath = ''
if (win.__POWERED_BY_QIANKUN__) {
  microPath = config.microPath
}
Vue.prototype.getMicroPath = function (path) {
  return path.replace('#', '#' + microPath).replace('\/\/', '/')
}
Vue.prototype.getMicroRoute = function (path) {
  return path.replace(/^\//, '/' + microPath + '/').replace('\/\/', '/')
}
var $dog = win.$dog
if ($dog) {
  for (let key in $dog) {
    dog[key] = $dog[key]
  }
}
win.$dog = $dog = dog
win.$dog.Vue = Vue
win.MonacoWorkerBaseUrl = origin + '/pool/prod/packages/monaco-editor/'

/* 原始启动代码 */
if (!win.__POWERED_BY_QIANKUN__) {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#micro-app')
}

export default {
  Vue, Vuex, VueRouter, router, store, App
}

export {
  Vue, Vuex, VueRouter, router, store, App
}