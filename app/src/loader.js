/**
 * @description 应用加载器
 * */

import $pig from '@daelui/pigjs'

const isProd = process.env.NODE_ENV === 'production'
let origin = location.origin
origin = isProd ? origin : '//www.daelui.cn'

export default $pig.init({
  registry: origin + '/pool/prod/components.json',
  root: origin + '/pool/prod/packages',
  versions: [{name: 'vue', version: '2.*.*'}]
}).then(function(){
  $pig.import(origin + '/df3s/tigerlair/app/static/css/main.css')
  $pig.import(origin + '/df3s/tigerlair/app/static/js/chunk-vendors.js')
  return $pig.import(origin + '/df3s/tigerlair/app/static/js/main.js')
})
