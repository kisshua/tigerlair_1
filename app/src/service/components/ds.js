/**
 * @description 数据请求组件
 * @since 2019-03-06
 * @author Rid King
*/

import { ds, CACHE_TYPE } from '@daelui/dogjs/dist/components'
import resolver from './resolver'

// 设置token
ds.$on('onBeforeRequest', function (options) {
  let token = localStorage.getItem('daelui-token')
  if (token) {
    options.headers.token = token
  }
})
// 设置解析器
ds.setOptions({ resolver })

export default ds

export {
  CACHE_TYPE
}
