/**
* @description 页面管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryTreeAll": {
		"url": "{host}/tigerlair/page/list/treeall",
		"desc": "查询页面列表",
		"method": "get"
	},
	"queryItem": {
		"url": "{host}/tigerlair/page/item",
		"desc": "查询页面",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/page/item",
		"desc": "删除页面",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/page/item",
		"desc": "编辑页面",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/page/item",
		"desc": "添加页面",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/page/list/page",
		"desc": "查询页面列表",
		"method": "get"
	},
	"queryNodeTree": {
		"url": "{host}/tigerlair/page/node/tree",
		"desc": "查询页面下的所有页面与功能",
		"method": "get"
	}
}
