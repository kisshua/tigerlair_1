/**
* @description 笔记类型(※自动化生成,勿手动更改※)
*/

export default {
	"removeItem": {
		"url": "{host}/tigerlair/noteType/item",
		"desc": "删除笔记分类",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/noteType/item",
		"desc": "编辑分类",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/noteType/item",
		"desc": "添加笔记分类",
		"method": "put"
	},
	"queryList": {
		"url": "{host}/tigerlair/noteType/list",
		"desc": "查询笔记类型列表",
		"method": "get"
	}
}
