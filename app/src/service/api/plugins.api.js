/**
* @description 插件管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/plugins/list/page",
		"desc": "查询插件列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/plugins/item",
		"desc": "添加插件",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/plugins/item",
		"desc": "编辑插件",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/plugins/item",
		"desc": "删除插件",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/plugins/item",
		"desc": "查询插件",
		"method": "get"
	}
}
