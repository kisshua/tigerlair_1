/**
* @description 采集分组管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/collect/inter/group/list/page",
		"desc": "查询分组列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/collect/inter/group/list/all",
		"desc": "查询所有列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/collect/inter/group/item",
		"desc": "添加分组",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/collect/inter/group/item",
		"desc": "编辑分组",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/collect/inter/group/item",
		"desc": "删除分组",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/collect/inter/group/item",
		"desc": "查询分组",
		"method": "get"
	}
}
