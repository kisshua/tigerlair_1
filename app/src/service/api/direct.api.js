/**
* @description 目录管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/direct/item",
		"desc": "查询目录",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/direct/item",
		"desc": "删除目录",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/direct/item",
		"desc": "编辑目录",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/direct/item",
		"desc": "添加目录",
		"method": "put"
	},
	"queryTree": {
		"url": "{host}/tigerlair/direct/list/tree",
		"desc": "查询目录列表",
		"method": "get"
	}
}
