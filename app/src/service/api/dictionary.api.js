/**
* @description 字典模块(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/dictionary/item",
		"desc": "查询字典项",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/dictionary/item",
		"desc": "删除字典项",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/dictionary/item",
		"desc": "编辑字典项",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/dictionary/item",
		"desc": "添加字典项",
		"method": "put"
	},
	"queryList": {
		"url": "{host}/tigerlair/dictionary/list/all",
		"desc": "查询字典列表",
		"method": "get"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/dictionary/list/page",
		"desc": "查询字典列表",
		"method": "get"
	},
	"queryTree": {
		"url": "{host}/tigerlair/dictionary/list/tree",
		"desc": "查询页面列表",
		"method": "get"
	}
}
