/**
* @description 视图页面管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryTreeAll": {
		"url": "{host}/tigerlair/viewpage/list/treeall",
		"desc": "查询页面列表",
		"method": "get"
	},
	"queryItem": {
		"url": "{host}/tigerlair/viewpage/item",
		"desc": "查询页面",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/viewpage/item",
		"desc": "删除页面",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/viewpage/item",
		"desc": "编辑页面",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/viewpage/item",
		"desc": "添加页面",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/viewpage/list/page",
		"desc": "查询页面列表",
		"method": "get"
	},
	"queryNodeTree": {
		"url": "{host}/tigerlair/viewpage/node/tree",
		"desc": "查询页面下的所有页面与功能",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/viewpage/list/all",
		"desc": "查询页面",
		"method": "get"
	}
}
