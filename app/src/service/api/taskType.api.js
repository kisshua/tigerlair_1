/**
* @description 任务类型(※自动化生成,勿手动更改※)
*/

export default {
	"removeItem": {
		"url": "{host}/tigerlair/taskType/item",
		"desc": "删除任务分类",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/taskType/item",
		"desc": "编辑分类",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/taskType/item",
		"desc": "添加任务分类",
		"method": "put"
	},
	"queryList": {
		"url": "{host}/tigerlair/taskType/list",
		"desc": "查询任务类型列表",
		"method": "get"
	}
}
