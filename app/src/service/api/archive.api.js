/**
* @description 文档管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/archive/list/page",
		"desc": "查询文档列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/archive/item",
		"desc": "添加文档",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/archive/item",
		"desc": "编辑文档",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/archive/item",
		"desc": "删除文档",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/archive/item",
		"desc": "查询文档",
		"method": "get"
	}
}
