/**
* @description 文件浏览器(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/explore/list/page",
		"desc": "查询文档列表",
		"method": "get"
	}
}
