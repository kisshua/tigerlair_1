/**
* @description 实体管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/entity/item",
		"desc": "查询实体",
		"method": "get"
	},
	"queryAll": {
		"url": "{host}/tigerlair/entity/list/all",
		"desc": "查询所有实体列表",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/entity/item",
		"desc": "删除实体",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/entity/item",
		"desc": "编辑实体",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/entity/item",
		"desc": "添加实体",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/entity/list/page",
		"desc": "查询实体列表",
		"method": "get"
	}
}
