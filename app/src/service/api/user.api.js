/**
* @description 用户管理(※自动化生成,勿手动更改※)
*/

export default {
	"login": {
		"url": "{host}/{pot}/user/login",
		"desc": "登录",
		"method": "post"
	},
	"logout": {
		"url": "{host}/{pot}/user/logout",
		"desc": "登出",
		"method": "post"
	},
	"queryUserInfo": {
		"url": "{host}/{pot}/user/queryUserInfo",
		"desc": "登录",
		"method": "get"
	}
}
