/**
* @description 导航管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/nav/list/page",
		"desc": "查询导航列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/nav/list/all",
		"desc": "查询导航列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/nav/item",
		"desc": "添加导航",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/nav/item",
		"desc": "编辑导航",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/nav/item",
		"desc": "删除导航",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/nav/item",
		"desc": "查询导航",
		"method": "get"
	},
	"queryListTree": {
		"url": "{host}/tigerlair/nav/list/tree",
		"desc": "查询导航下的所有导航",
		"method": "get"
	}
}
