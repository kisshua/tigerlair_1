/**
* @description 代理管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/proxier/list/page",
		"desc": "查询代理列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/proxier/item",
		"desc": "添加代理",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/proxier/item",
		"desc": "编辑代理",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/proxier/item",
		"desc": "删除代理",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/proxier/item",
		"desc": "查询代理",
		"method": "get"
	}
}
