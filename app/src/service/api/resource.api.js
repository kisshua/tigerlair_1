/**
* @description 资源目录管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/resource/list/page",
		"desc": "查询数据源列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/resource/item",
		"desc": "添加数据源",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/resource/item",
		"desc": "编辑数据源",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/resource/item",
		"desc": "删除数据源",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/resource/item",
		"desc": "查询数据源",
		"method": "get"
	}
}
