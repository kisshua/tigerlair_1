/**
* @description 导航管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryTree": {
		"url": "{host}/tigerlair/nave/list/tree",
		"desc": "查询页面列表",
		"method": "get"
	},
	"queryTreeAll": {
		"url": "{host}/tigerlair/nave/list/treeall",
		"desc": "查询页面列表",
		"method": "get"
	},
	"queryItem": {
		"url": "{host}/tigerlair/nave/item",
		"desc": "查询页面",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/nave/item",
		"desc": "删除页面",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/nave/item",
		"desc": "编辑页面",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/nave/item",
		"desc": "添加页面",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/nave/list/page",
		"desc": "查询页面列表",
		"method": "get"
	}
}
