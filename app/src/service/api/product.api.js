/**
* @description 产品管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/product/list/page",
		"desc": "查询产品列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/product/list/all",
		"desc": "查询产品列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/product/item",
		"desc": "添加产品",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/product/item",
		"desc": "编辑产品",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/product/item",
		"desc": "删除产品",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/product/item",
		"desc": "查询产品",
		"method": "get"
	},
	"queryNodeTree": {
		"url": "{host}/tigerlair/product/node/tree",
		"desc": "查询产品下的所有页面与功能",
		"method": "get"
	}
}
