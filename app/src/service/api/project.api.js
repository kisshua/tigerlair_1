/**
* @description 项目管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/project/list/page",
		"desc": "查询项目列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/project/item",
		"desc": "添加项目",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/project/item",
		"desc": "编辑项目",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/project/item",
		"desc": "删除项目",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/project/item",
		"desc": "查询项目",
		"method": "get"
	}
}
