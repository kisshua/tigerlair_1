/**
* @description 任务(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/task/item",
		"desc": "查询任务",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/task/item",
		"desc": "删除任务",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/task/item",
		"desc": "编辑任务",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/task/item",
		"desc": "添加任务",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/task/list/page",
		"desc": "查询任务列表",
		"method": "get"
	}
}
