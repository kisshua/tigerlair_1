/**
* @description 文档文件(※自动化生成,勿手动更改※)
*/

export default {
	"queryListAll": {
		"url": "{host}/tigerlair/arcfile/list/all",
		"desc": "查询文档列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/arcfile/item",
		"desc": "添加文档",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/arcfile/item",
		"desc": "编辑文档",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/arcfile/item",
		"desc": "删除文档",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/arcfile/item",
		"desc": "查询文档",
		"method": "get"
	}
}
