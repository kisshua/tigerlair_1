/**
* @description 编码库管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryTree": {
		"url": "{host}/tigerlair/codewell/list/tree",
		"desc": "查询编码库列表",
		"method": "get"
	},
	"queryItem": {
		"url": "{host}/tigerlair/codewell/item",
		"desc": "查询编码库",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/codewell/item",
		"desc": "删除编码库",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/codewell/item",
		"desc": "编辑编码库",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/codewell/item",
		"desc": "添加编码库",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/codewell/list/page",
		"desc": "查询编码库列表",
		"method": "get"
	},
	"queryNodeTree": {
		"url": "{host}/tigerlair/codewell/node/tree",
		"desc": "查询库下的所有文档",
		"method": "get"
	}
}
