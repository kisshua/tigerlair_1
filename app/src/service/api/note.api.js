/**
* @description 笔记管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/note/list/page",
		"desc": "查询笔记列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/note/item",
		"desc": "添加笔记",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/note/item",
		"desc": "编辑笔记",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/note/item",
		"desc": "删除笔记",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/note/item",
		"desc": "查询笔记",
		"method": "get"
	},
	"queryListOptions": {
		"url": "{host}/tigerlair/note/list/options",
		"desc": "查询笔记菜单",
		"method": "get"
	}
}
