/**
* @description 服务管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/inter/list/page",
		"desc": "查询服务列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/inter/item",
		"desc": "添加服务",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/inter/item",
		"desc": "编辑服务",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/inter/item",
		"desc": "删除服务",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/inter/item",
		"desc": "查询服务",
		"method": "get"
	}
}
