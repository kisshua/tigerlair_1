/**
* @description 页面管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/section/list/page",
		"desc": "查询页面列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/section/item",
		"desc": "添加页面",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/section/item",
		"desc": "编辑页面",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/section/item",
		"desc": "删除页面",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/section/item",
		"desc": "查询页面",
		"method": "get"
	}
}
