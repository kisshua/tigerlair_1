/**
* @description 处理单元管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/exeunit/list/page",
		"desc": "查询处理单元列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/exeunit/item",
		"desc": "添加处理单元",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/exeunit/item",
		"desc": "编辑处理单元",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/exeunit/item",
		"desc": "删除处理单元",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/exeunit/item",
		"desc": "查询处理单元",
		"method": "get"
	}
}
