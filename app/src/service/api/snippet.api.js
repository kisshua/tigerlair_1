/**
* @description 片段管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryItem": {
		"url": "{host}/tigerlair/snippet/item",
		"desc": "查询编码库",
		"method": "get"
	},
	"removeItem": {
		"url": "{host}/tigerlair/snippet/item",
		"desc": "删除编码库",
		"method": "delete"
	},
	"editItem": {
		"url": "{host}/tigerlair/snippet/item",
		"desc": "编辑编码库",
		"method": "post"
	},
	"addItem": {
		"url": "{host}/tigerlair/snippet/item",
		"desc": "添加编码库",
		"method": "put"
	},
	"queryListPage": {
		"url": "{host}/tigerlair/snippet/list/page",
		"desc": "查询编码库列表",
		"method": "get"
	}
}
