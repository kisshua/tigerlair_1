/**
* @description 文件管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/dfile/list/page",
		"desc": "查询文件列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/dfile/item",
		"desc": "添加文件",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/dfile/item",
		"desc": "编辑文件",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/dfile/item",
		"desc": "删除文件",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/dfile/item",
		"desc": "查询文件",
		"method": "get"
	},
	"deploy": {
		"url": "{host}/tigerlair/dfile/deploy",
		"desc": "部署文件",
		"method": "post"
	}
}
