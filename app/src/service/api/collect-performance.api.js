/**
* @description 报错管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/collect/performance/list/page",
		"desc": "查询报错列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/collect/performance/item",
		"desc": "添加报错",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/collect/performance/item",
		"desc": "编辑报错",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/collect/performance/item",
		"desc": "删除报错",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/collect/performance/item",
		"desc": "查询报错",
		"method": "get"
	},
	"store": {
		"url": "{host}/tigerlair/collect/performance/store",
		"desc": "保存报错",
		"method": "post"
	}
}
