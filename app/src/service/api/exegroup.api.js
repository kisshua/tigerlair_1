/**
* @description 流程组管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/exegroup/list/page",
		"desc": "查询流程组列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/exegroup/item",
		"desc": "添加流程组",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/exegroup/item",
		"desc": "编辑流程组",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/exegroup/item",
		"desc": "删除流程组",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/exegroup/item",
		"desc": "查询流程组",
		"method": "get"
	}
}
