/**
* @description 文档目录管理(※自动化生成,勿手动更改※)
*/

export default {
	"queryListPage": {
		"url": "{host}/tigerlair/chapter/list/page",
		"desc": "查询文档目录列表",
		"method": "get"
	},
	"queryListAll": {
		"url": "{host}/tigerlair/chapter/list/all",
		"desc": "查询文档目录列表",
		"method": "get"
	},
	"addItem": {
		"url": "{host}/tigerlair/chapter/item",
		"desc": "添加文档目录",
		"method": "put"
	},
	"editItem": {
		"url": "{host}/tigerlair/chapter/item",
		"desc": "编辑文档目录",
		"method": "post"
	},
	"removeItem": {
		"url": "{host}/tigerlair/chapter/item",
		"desc": "删除文档目录",
		"method": "delete"
	},
	"queryItem": {
		"url": "{host}/tigerlair/chapter/item",
		"desc": "查询文档目录",
		"method": "get"
	},
	"queryNodeTree": {
		"url": "{host}/tigerlair/chapter/node/tree",
		"desc": "查询文档目录下的所有页面与功能",
		"method": "get"
	}
}
