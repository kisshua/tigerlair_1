/**
 * @description 数据修改
*/

import state from './state.js'
import { strer } from '@daelui/dogjs/dist/components'

export default {
  // 设置布局集合
  setLayoutCollection (list) {
    list = Array.isArray(list) ? list : []
    let collection = state.layoutCollection
    list.forEach(item => {
      let node = collection.find(node => node.name === item.name && node.version === item.version)
      if (!node) {
        state.layoutCollection.push(item)
      } else {
        Object.assign(node, item)
      }
    })
  },
  // 设置组件集合
  setComponentCollection (list) {
    list = Array.isArray(list) ? list : []
    let collection = state.componentCollection
    list.forEach(item => {
      let node = collection.find(node => node.name === item.name && node.version === item.version)
      if (!node) {
        state.componentCollection.push(item)
      } else {
        Object.assign(node, item)
      }
    })
  },
  // 选中的属性组件
  selectPropertyView (view) {
    state.layoutPropertyView = view
  },
  // 选中布局
  selectLayout (meta, defProps) {
    state.selectedLayout = meta
    if (defProps) {
      state.selectedLayoutDefProps = defProps
    } else {
      state.selectedLayoutDefProps = {}
    }
  },
  // 新增组件
  insertComponent (meta) {
    state.page.meta.childComponents.push({
      id: strer.utid(),
      name: meta.name,
      version: meta.version,
      properts: {},
      layout: { x: 0, y: 0, w: 16, h: 16 },
      events: {},
      theme: {},
      childComponents: []
    })
  },
  // 移除组件
  removeComponent (meta) {
    state.page.meta.childComponents = state.page.meta.childComponents.filter(item => {
      return item !== meta
    })
  },
  // 选中组件
  selectComponent (meta) {
    state.selectedComponent = meta
  }
}