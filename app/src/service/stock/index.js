/**
 * @description 数据
*/

import state from './state.js'
import getter from './getter.js'
import setter from './setter.js'
import action from './action.js'

export default {
  state, getter, setter, action
}

export {
  state, getter, setter, action
}
