/**
 * @description 数据
*/

import { DEnum, OptionEnum } from '@daelui/dogjs/dist/components'

export default {
  // 忽略目录
  ignoreDir: 'node_modules;npm-cache;.git;.cache;_cacache',
  // 目录类型
  directTypes: new DEnum([
    {key: 'DIR', text: '系统目录'},
    {key: 'DIR_SIMULATE', text: '模拟目录'},
    {key: 'FILE', text: '文件'}
  ]),
  // 启用与禁用状态
  enableStatus: new OptionEnum([
    {key: 'ENABLE', id: 1, text: '启用'},
    {key: 'DISABLE', id: 0, text: '禁用'}
  ], { valueKey: 'id' }),
  // 状态列表
  statusList: [
    {id: 1, value: 1, text: '启用'},
    {id: 0, calue: 0, text: '禁用'}
  ],
  // 判断状态
  judgeOptions: new OptionEnum([
    {key: 'TRUE', id: 1, text: '是'},
    {key: 'FALSE', id: 0, text: '否'}
  ], { hasAll: false, valueKey: 'id' }),
  // 页面类型
  pageTypeList: new OptionEnum([
    {key: 'NORMAL', id: 1, text: '普通'},
    {key: 'RESOURCE_PAGE', id: 2, text: '引用'},
    {key: 'ENVIRMENT_MATCH', id: 3, text: '环境匹配'}
  ], { hasAll: false, valueKey: 'id' }),
  // 页签风格
  tabTypeList: new OptionEnum([
    {key: 'underline', text: '下划线'},
    {key: 'beforeline', text: '上划线'},
    {key: 'leftline', text: '左划线'},
    {key: 'rightline', text: '右划线'}
  ], { hasAll: false }),
  // 页签位置
  tabPositionList: new OptionEnum([
    {key: 'top', text: '头部'},
    {key: 'side-left', text: '左侧'},
    {key: 'side-right', text: '右侧'},
    {key: 'bottom', text: '底部'}
  ], { hasAll: false }),
  // 文档分类类型
  archiveCatList: new OptionEnum([
    {key: 'ARCHIVE', id: 1, value: 1, text: '一般文档'},
    {key: 'CODEWELL', id: 2, value: 2, text: '编码库文档'},
    {key: 'CHAPTER', id: 3, value: 3, text: '目录文档'}
  ]),
  // 文档类型
  docTypeList: [
    {id: 1, value: 1, text: '使用文档'},
    {id: 2, value: 2, text: '开发文档'}
  ],
  // 数据源类型
  resourceTypeList: new OptionEnum([
    {key: 'JAVASCRIPT', id: 'js', text: 'JavaScript'},
    {key: 'CSS', id: 'css', text: 'CSS'},
    {key: 'TXT', id: 'txt', text: '文本'},
    // {key: 'COMPRESS', id: 1, text: '压缩包'},
    // {key: 'FILE', id: 2, text: '文件'},
    // {key: 'SERVICE', id: 4, text: '服务请求'}
  ], { hasAll: false, valueKey: 'id', enumValueKey: 'id' }),
  // 执行单元类型
  exeunitTypeList: new OptionEnum([
    {key: 'RESOURCE', id: 1, text: '数据源'},
    {key: 'COMPUTE', id: 2, text: '计算'},
    {key: 'VIEW', id: 3, text: '视图'},
    {key: 'STORE', id: 4, text: '存储'}
  ], { hasAll: false, valueKey: 'id' }),
  // 执行方式
  runTypeList: new OptionEnum([
    {key: 'COMPONENT', id: 1, text: '组件'},
    {key: 'CODE', id: 2, text: '自定义编码'}
  ], { hasAll: false, valueKey: 'id' }),
  // 接口采集匹配类型
  collectInterFieldsList: new OptionEnum([
    {key: 'URL', id: 'url', text: '接口地址'},
    {key: 'HREF', id: 'href', text: '页面地址'}
  ], { hasAll: false, valueKey: 'id' }),
  // 报错采集匹配类型
  collectErrorFieldsList: new OptionEnum([
    {key: 'HREF', id: 'href', text: '页面地址'},
    {key: 'NAME', id: 'name', text: '错误名称'},
    {key: 'MESSAGE', id: 'message', text: '报错信息'},
    {key: 'FILENAME', id: 'fileName', text: '文件名称'},
    {key: 'URL', id: 'url', text: '文件地址'},
    {key: 'STACK', id: 'stack', text: '错误栈'},
    {key: 'TIME', id: 'time', text: '时间点'},
    {key: 'PAGEPROTOCOL', id: 'pageProtocol', text: '协议类型'},
    {key: 'PAGEHOST', id: 'pageHost', text: '主机地址'},
    {key: 'PAGEPATHNAME', id: 'pagePathname', text: '路径'},
    {key: 'USERAGENT', id: 'userAgent', text: '浏览器'},
    {key: 'LANGUAGE', id: 'language', text: '语言'},
    {key: 'PLATFORM', id: 'platform', text: '平台'}
  ], { hasAll: false, valueKey: 'id' }),
  // 性能采集匹配类型
  collectPerformanceFieldsList: new OptionEnum([
    {key: 'HREF', id: 'href', text: '页面地址'},
    {key: 'TIME', id: 'time', text: '时间点'},
    {key: 'PAGEPROTOCOL', id: 'pageProtocol', text: '协议类型'},
    {key: 'PAGEHOST', id: 'pageHost', text: '主机地址'},
    {key: 'PAGEPATHNAME', id: 'pagePathname', text: '路径'},
    {key: 'USERAGENT', id: 'userAgent', text: '浏览器'},
    {key: 'LANGUAGE', id: 'language', text: '语言'},
    {key: 'PLATFORM', id: 'platform', text: '平台'}
  ], { hasAll: false, valueKey: 'id' }),
  // 流程可视组件列表
  visualList: [],
  // 自定义单元
  customUnit: {
    key: 'CUSTOM',
    id: '-1',
    name: '自定义单元',
    executor: `export default function (payload) {
      return payload
}`,
    input: `export default function (payload) {
  return payload
}`,
    output: `export default function (payload) {
  return payload
}`
  },
  codeLangTypeList: new OptionEnum([
    {key: 'VUE', id: 'vue', text: 'VUE'}
  ], { hasAll: false, valueKey: 'id' }),
  registerTypeList: new OptionEnum([
    {key: 'component', id: 'component', text: '组件'},
    {key: 'package', id: 'package', text: '资源包'},
    {key: 'runtime', id: 'runtime', text: '运行环境'}
  ], { hasAll: false, valueKey: 'id' }),
  // 片段类型
  snippetTypeList: new OptionEnum([
    {key: 'HTML', id: 'html', text: 'HTML'},
    {key: 'VUE', id: 'vue', text: 'VUE'}
  ], { hasAll: false, valueKey: 'id' }),
  // 布局列表
  layoutCollection: [],
  // 组件模板列表
  componentCollection: [],
  // 组件列表
  components: [],
  // 页面数据
  page: {
    meta: { name: '' }
  },
  // 布局渲染组件
  layoutRenderView: '',
  // 布局属性组件
  layoutPropertyView: '',
  // 布局组件属性
  layoutPropertyStock: {},
  // 选中的布局
  selectedLayout: {},
  // 选中的布局默认属性
  selectedLayoutDefProps: {},
  // 是否设计状态
  isDesign: false,
  // 是否对照状态
  isContrast: false,
  // 选中的组件
  selectedComponent: {}
}
