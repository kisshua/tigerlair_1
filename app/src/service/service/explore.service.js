/**
* @description 文件浏览器(※自动化生成,勿手动更改※)
*/

import ds from '../components/ds'
import resolver from '../components/resolver'
import API from '../api/explore.api'

// api地址解析
const api = resolver.solveAPI(API)

export default {
  /**
   * @function 查询文档列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListPage (...params) {
    return ds.get(api.queryListPage.url, ...params, {})
  }
}

export {
  api, ds
}
