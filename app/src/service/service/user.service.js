/**
* @description 用户管理(※自动化生成,勿手动更改※)
*/

import ds from '../components/ds'
import resolver from '../components/resolver'
import API from '../api/user.api'

// api地址解析
const api = resolver.solveAPI(API)

export default {
  /**
   * @function 登录
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  login (...params) {
    return ds.post(api.login.url, ...params, {})
  },
  /**
   * @function 登出
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  logout (...params) {
    return ds.post(api.logout.url, ...params, {})
  },
  /**
   * @function 获取用户信息
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryUserInfo (...params) {
    return ds.get(api.queryUserInfo.url, ...params, {})
  }
}

export {
  api, ds
}
