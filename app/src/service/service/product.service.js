/**
* @description 产品管理(※自动化生成,勿手动更改※)
*/

import ds from '../components/ds'
import resolver from '../components/resolver'
import API from '../api/product.api'

// api地址解析
const api = resolver.solveAPI(API)

export default {
  /**
   * @function 查询产品列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListPage (...params) {
    return ds.get(api.queryListPage.url, ...params, {})
  },
  /**
   * @function 查询产品列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListAll (...params) {
    return ds.get(api.queryListAll.url, ...params, {})
  },

  /**
   * @function 添加产品
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  addItem (...params) {
    return ds.put(api.addItem.url, ...params, {})
  },

  /**
   * @function 编辑产品
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  editItem (...params) {
    return ds.post(api.editItem.url, ...params, {})
  },

  /**
   * @function 删除产品
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  removeItem (...params) {
    return ds.delete(api.removeItem.url, ...params, {})
  },

  /**
   * @function 查询产品
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryItem (...params) {
    return ds.get(api.queryItem.url, ...params, {})
  },

  /**
   * @function 查询产品下的所有页面与功能
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryNodeTree (...params) {
    return ds.get(api.queryNodeTree.url, ...params, {})
  }
}

export {
  api, ds
}
