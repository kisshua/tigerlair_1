/**
* @description 字典模块(※自动化生成,勿手动更改※)
*/

import ds from '../components/ds'
import resolver from '../components/resolver'
import API from '../api/dictionary.api'

// api地址解析
const api = resolver.solveAPI(API)

export default {
  /**
   * @function 查询字典项
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryItem (...params) {
    return ds.get(api.queryItem.url, ...params, {})
  },

  /**
   * @function 删除字典项
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  removeItem (...params) {
    return ds.delete(api.removeItem.url, ...params, {})
  },

  /**
   * @function 编辑字典项
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  editItem (...params) {
    return ds.post(api.editItem.url, ...params, {})
  },

  /**
   * @function 添加任务字典项
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  addItem (...params) {
    return ds.put(api.addItem.url, ...params, {})
  },

  /**
   * @function 查询字典列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryList (...params) {
    return ds.get(api.queryList.url, ...params, {})
  },

  /**
   * @function 查询页面列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryTree (...params) {
    return ds.get(api.queryTree.url, ...params, {})
  }
}

export {
  api, ds
}
