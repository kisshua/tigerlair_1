/**
* @description 笔记管理(※自动化生成,勿手动更改※)
*/

import ds from '../components/ds'
import resolver from '../components/resolver'
import API from '../api/note.api'

// api地址解析
const api = resolver.solveAPI(API)

export default {
  /**
   * @function 查询笔记列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListPage (...params) {
    return ds.get(api.queryListPage.url, ...params, {})
  },

  /**
   * @function 添加笔记
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  addItem (...params) {
    return ds.put(api.addItem.url, ...params, {})
  },

  /**
   * @function 编辑笔记
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  editItem (...params) {
    return ds.post(api.editItem.url, ...params, {})
  },

  /**
   * @function 删除笔记
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  removeItem (...params) {
    return ds.delete(api.removeItem.url, ...params, {})
  },

  /**
   * @function 查询笔记
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryItem (...params) {
    return ds.get(api.queryItem.url, ...params, {})
  },
  /**
   * @function 查询笔记列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListOptions (...params) {
    return Promise.resolve({
      code: '',
      data: [],
      msg: 'success',
      status: 200,
      success: 1,
      url: ''
    }) // ds.get(api.queryListOptions.url, ...params, {})
  }
}

export {
  api, ds
}
