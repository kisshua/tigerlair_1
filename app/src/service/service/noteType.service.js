/**
* @description 笔记类型(※自动化生成,勿手动更改※)
*/

import ds from '../components/ds'
import resolver from '../components/resolver'
import API from '../api/noteType.api'

// api地址解析
const api = resolver.solveAPI(API)

export default {
  /**
   * @function 删除笔记分类
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  removeItem (...params) {
    return ds.delete(api.removeItem.url, ...params, {})
  },

  /**
   * @function 编辑分类
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  editItem (...params) {
    return ds.post(api.editItem.url, ...params, {})
  },

  /**
   * @function 添加笔记分类
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  addItem (...params) {
    return ds.put(api.addItem.url, ...params, {})
  },

  /**
   * @function 查询笔记类型列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryList (...params) {
    return ds.get(api.queryList.url, ...params, {})
  }
}

export {
  api, ds
}
