/**
* @description 文档管理(※自动化生成,勿手动更改※)
*/

import ds from '../components/ds'
import resolver from '../components/resolver'
import API from '../api/doc.api'

// api地址解析
const api = resolver.solveAPI(API)

export default {
  /**
   * @function 取消文件内容
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  readFile (...params) {
    return ds.get(api.readFile.url, ...params, {})
  },

  /**
   * @function 下载文件
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  downloadFile (...params) {
    return ds.get(api.downloadFile.url, ...params, {})
  },

  /**
   * @function 打开文件所有目录
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  openFileDir (...params) {
    return ds.get(api.openFileDir.url, ...params, {})
  },

  /**
   * @function 查询文档
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryItem (...params) {
    return ds.get(api.queryItem.url, ...params, {})
  },

  /**
   * @function 删除文档
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  removeItem (...params) {
    return ds.delete(api.removeItem.url, ...params, {})
  },

  /**
   * @function 编辑文档
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  editItem (...params) {
    return ds.post(api.editItem.url, ...params, {})
  },

  /**
   * @function 添加文档
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  addItem (...params) {
    return ds.put(api.addItem.url, ...params, {})
  },

  /**
   * @function 查询文档列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListPage (...params) {
    return ds.get(api.queryListPage.url, ...params, {})
  }
}

export {
  api, ds
}
