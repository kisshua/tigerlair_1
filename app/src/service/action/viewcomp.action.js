/**
* @description 视图组件管理
*/

import Vue from 'vue'
import ds from '../components/ds'
import service, {api} from '../service/viewcomp.service'
import { coder } from '@daelui/dogjs/dist/components'

// 服务
export default Object.assign({}, service, {
  queryAllListOption (isAll) {
    // 查询文档列表
    return service.queryListPage({pageSize: 1000}).then(res => {
      let list = res.data.list
      list = Array.isArray(list) ? list : []
      list = list.map(item => {
        return {
          id: item.id,
          value: item.id,
          text: item.name
        }
      })
      if (isAll) {
        list.unshift({
          id: '',
          value: '',
          text: '全部'
        })
      }
      return list
    })
  },

  // 查询所有视图组件
  queryAllComponents () {
    return this.queryListAll().then(res => {
      let data = res.data
      data = Array.isArray(data) ? data : []
      let cps = data.map(item => {
        return {
          id: item.id,
          name: item.name,
          text: item.text,
          version: item.version,
          icon: item.icon,
          description: item.description,
          component: function () {
            return coder.vue.resolve(item.viewCode, Vue, $pig)
          },
          url: item.url,
          propertyComponent: function () {
            return coder.vue.resolve(item.propertyCode, Vue, $pig)
          },
          propertyUrl: item.propertyUrl
        }
      })
      return cps
    })
  }
})

export {
  api, service, ds
}
