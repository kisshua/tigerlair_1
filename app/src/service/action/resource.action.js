/**
* @description 数据源管理
*/

import ds from '../components/ds'
import service, {api} from '../service/resource.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
