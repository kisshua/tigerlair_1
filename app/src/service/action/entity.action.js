/**
* @description 页面管理
*/

import ds from '../components/ds'
import service, {api} from '../service/entity.service'

// 服务
export default Object.assign({}, service, {
  queryAllListOption (params) {
    params = params || {}
    // 查询文档列表
    return service.queryAll({...params}).then(res => {
      let list = res.data
      list = Array.isArray(list) ? list : []
      list = list.map(item => {
        return {
          id: item.id,
          value: item.id,
          text: item.entity + (item.name ? '(' + item.name + ')' : item.name)
        }
      })
      return list
    })
  }
})

export {
  api, service, ds
}
