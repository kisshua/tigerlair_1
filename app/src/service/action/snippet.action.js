/**
* @description 片段管理
*/

import ds from '../components/ds'
import service, {api} from '../service/snippet.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
