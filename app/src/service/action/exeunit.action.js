/**
* @description 处理单元管理
*/

import ds from '../components/ds'
import service, {api} from '../service/exeunit.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
