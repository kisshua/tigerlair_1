/**
* @description 导航管理
*/

import ds from '../components/ds'
import service, {api} from '../service/nave.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
