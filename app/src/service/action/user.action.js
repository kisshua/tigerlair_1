/**
* @description 用户管理
*/

import ds from '../components/ds'
import service, {api} from '../service/user.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
