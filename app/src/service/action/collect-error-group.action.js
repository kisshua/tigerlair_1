/**
* @description 报错分组管理
*/

import ds from '../components/ds'
import service, {api} from '../service/collect-error-group.service'

// 服务
export default Object.assign({}, service, {
  queryListAll ({ hasAll }) {
    return service.queryListAll().then(res => {
      let list = res.data
      list = Array.isArray(list) ? list : []
      list = list.map(item => {
        return {
          id: item.id,
          value: item.id,
          text: item.name
        }
      })
      if (hasAll) {
        list.unshift({id: '', value: '', text: '全部'})
      }
      return list
    })
  }
})

export {
  api, service, ds
}
