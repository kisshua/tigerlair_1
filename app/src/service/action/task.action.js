/**
* @description 任务
*/

import ds from '../components/ds'
import service, {api} from '../service/task.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
