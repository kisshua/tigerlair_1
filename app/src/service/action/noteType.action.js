/**
* @description 笔记类型
*/

import ds from '../components/ds'
import service, {api} from '../service/noteType.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
