/**
* @description 任务类型
*/

import ds from '../components/ds'
import service, {api} from '../service/taskType.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
