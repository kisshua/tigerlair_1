/**
* @description 文件管理
*/

import ds from '../components/ds'
import service, {api} from '../service/dfile.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
