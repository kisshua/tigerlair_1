/**
* @description 文件浏览器
*/

import ds from '../components/ds'
import service, {api} from '../service/explore.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
