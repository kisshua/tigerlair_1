/**
* @description 采集报错管理
*/

import ds from '../components/ds'
import service, {api} from '../service/collect-performance.service'

// 服务
export default Object.assign({}, service, {
  /**
   * @function 查询报错列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryListPage () {
    return service.queryListPage.apply(service, arguments).then(res => {
      res.data = res.data || {}
      res.data.list = Array.isArray(res.data.list) ? res.data.list : []
      res.data.list.forEach(item => {
        let data = item
        let reqEnd = data.reqEnd, reqStart = data.reqStart
        reqEnd = parseFloat(reqEnd)
        reqStart = parseFloat(reqStart)
        if (!data.resTime && !isNaN(reqEnd) && !isNaN(reqStart)) {
          data.resTime = reqEnd - reqStart
        }
      })
      return res
    })
  },

  /**
   * @function 添加报错
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryItem () {
    return service.queryItem.apply(service, arguments).then(res => {
      res.data = res.data || {}
      let data = res.data
      let reqEnd = data.reqEnd, reqStart = data.reqStart
      reqEnd = parseFloat(reqEnd)
      reqStart = parseFloat(reqStart)
      if (!data.resTime && !isNaN(reqEnd) && !isNaN(reqStart)) {
        data.resTime = reqEnd - reqStart
      }
      return res
    })
  },

  /**
   * @function 添加报错
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  addItem (data) {
    data = data || {}
    return service.addItem.apply(service, [data])
  },

  /**
   * @function 保存报错
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  store (data) {
    data = data || {}
    return service.store.apply(service, [data])
  }
})

export {
  api, service, ds
}
