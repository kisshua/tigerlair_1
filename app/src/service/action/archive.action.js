/**
* @description 文档管理
*/

import ds from '../components/ds'
import service, { api } from '../service/archive.service'

// 服务
export default Object.assign({}, service, {
  queryAllListOption (params) {
    params = params || {}
    // 查询文档列表
    return service.queryListPage({pageSize: 1000, ...params}).then(res => {
      let list = res.data.list
      list = Array.isArray(list) ? list : []
      list = list.map(item => {
        return {
          id: item.id,
          value: item.id,
          text: item.name
        }
      })
      return list
    })
  }
})

export {
  api, service, ds
}
