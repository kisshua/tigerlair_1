/**
* @description 目录管理
*/

import ds from '../components/ds'
import service, {api} from '../service/direct.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
