/**
* @description 笔记管理
*/

import ds from '../components/ds'
import service, {api} from '../service/note.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
