/**
* @description 文档管理
*/

import ds from '../components/ds'
import service, {api} from '../service/doc.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
