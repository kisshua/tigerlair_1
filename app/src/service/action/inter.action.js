/**
* @description 服务管理
*/

import ds from '../components/ds'
import service, {api} from '../service/inter.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
