/**
* @description 文档目录管理
*/

import ds from '../components/ds'
import service, {api} from '../service/chapter.service'

// 服务
export default Object.assign({}, service, {
  queryListAllOption (isAll) {
    // 查询文档列表
    return service.queryListAll().then(res => {
      let list = res.data
      list = Array.isArray(list) ? list : []
      list = list.map(item => {
        return {
          id: item.id,
          value: item.id,
          text: item.name
        }
      })
      if (isAll) {
        list.unshift({
          id: '',
          value: '',
          text: '全部'
        })
      }
      return list
    })
  }
})

export {
  api, service, ds
}
