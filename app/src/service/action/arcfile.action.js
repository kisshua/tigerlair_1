/**
* @description 文档文件
*/

import ds from '../components/ds'
import service, {api} from '../service/arcfile.service'

// 服务
export default Object.assign({}, service, {

})

export {
  api, service, ds
}
