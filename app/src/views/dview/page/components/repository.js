import { state } from '@/service/stock'

/**
 * @function 获取展示组件
*/
const getComponent = function (options) {
  let component = getCP(options)
  return {
    component: component.component,
    name: component.name,
    version: component.version
  }
}

/**
 * @function 获取属性组件
*/
const getPropertyComponent = function (options) {
  let component = getCP(options)
  component = component || {}
  return {
    component: component.propertyComponent,
    name: component.name,
    version: component.version
  }
}

// 获取组件对象
const getCP = function (options) {
  options = options || {}
  let components = [...state.layoutCollection, ...state.componentCollection].filter(item => {
    return item.name === options.name
  })
  let component = components.find(item => {
    return item.version === options.version
  })
  component = component || components[0]
  component = component || { component: 'div', name: 'div', version: '1.0.0' }

  return component
}

export default {
  getComponent, getPropertyComponent
}

export {
  getComponent, getPropertyComponent
}