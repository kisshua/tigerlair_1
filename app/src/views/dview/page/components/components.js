/**
 * @description 视图组件列表
*/

const componentCollection = [
  {
    id: 1,
    name: 'ddesign-vue-template',
    text: 'Vue模板',
    version: '1.0.0',
    icon: '',
    description: '',
    component: resolve => require(['./vue/index.vue'], resolve),
    url: '',
    propertyComponent: resolve => require(['./vue/property.vue'], resolve),
    propertyUrl: '',
    repository: {
      url: '{host}/name/verion/index.js',
      propertyUrl: '{host}/name/verion/property.js'
    },
    category: '基础'
  },
  {
    id: 2,
    name: 'ddesign-screen-template',
    text: '大屏Vue模板',
    version: '1.0.0',
    icon: '',
    description: '',
    component: resolve => require(['./screen/index.vue'], resolve),
    url: '',
    propertyComponent: resolve => require(['./screen/property.vue'], resolve),
    propertyUrl: '',
    repository: {
      url: '{host}/name/verion/index.js',
      propertyUrl: '{host}/name/verion/property.js'
    },
    category: '基础'
  }
]

export default componentCollection