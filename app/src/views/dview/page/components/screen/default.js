export default {
  code: `<template>
  <div
    :class="{ 'is-show-angle': value.isShowAngle, 'is-screen-bg': value.isShowBg }"
    class="dog-screen-panel"
  >
    <div v-if="value.isShowHead" class="screen-panel-head">
      <div :title="value.name" class="head-title">
        <label>{{ value.name }}</label>
      </div>
    </div>
    <div class="screen-panel-body">
      <div class="panel-content"></div>
    </div>
    <div class="border-foot"></div>
  </div>
</template>

<script>
const props = {
  name: '大屏Vue模板',
  isShowBg: true, // 显示背景
  isShowAngle: true, // 显示四角
  isShowHead: true // 显示头
}
export default {
  props: {
    value: {
      type: Object,
      default() {
        return Object.assign({}, props)
      }
    }
  },

  created() {
    if (this.value.name === undefined) {
      Object.assign(this.value, props)
    }
  },

  mounted() {
    this.initView()
  },

  methods: {
    // 初始化视图
    initView() {}
  }
}
</script>

<style scoped>
.dog-screen-panel {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  box-shadow: inset 0 0 30px #07417a;
}
.dog-screen-panel .screen-panel-head {
  padding: 0 16px;
  width: 100%;
}
.screen-panel-head .head-title {
  height: 48px;
  font-size: 1.4rem;
  color: #fff;
  text-align: center;
  line-height: 48px;
  border-bottom: 1px solid rgba(255, 255, 255, 0.2);
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
}
.dog-screen-panel .screen-panel-body {
  height: 100%;
}
.screen-panel-head + .screen-panel-body {
  height: calc(100% - 48px);
}
.screen-panel-body .panel-content {
  padding: 8px 16px;
  color: #fff;
}
.dog-screen-panel.is-screen-bg {
  background: #000c3b;
}
.dog-screen-panel.is-show-angle:before {
  position: absolute;
  width: 1rem;
  height: 1rem;
  content: '';
  border-top: 2px solid #26c6f0;
  border-left: 2px solid #26c6f0;
  left: -1px;
  top: -1px;
}
.dog-screen-panel.is-show-angle:after {
  position: absolute;
  width: 1rem;
  height: 1rem;
  content: '';
  border-top: 2px solid #26c6f0;
  border-right: 2px solid #26c6f0;
  right: -1px;
  top: -1px;
}
.dog-screen-panel.is-show-angle .border-foot:before {
  position: absolute;
  width: 1rem;
  height: 1rem;
  content: '';
  border-bottom: 2px solid #26c6f0;
  border-left: 2px solid #26c6f0;
  left: -1px;
  bottom: -1px;
}
.dog-screen-panel.is-show-angle .border-foot:after {
  position: absolute;
  width: 1rem;
  height: 1rem;
  content: '';
  border-bottom: 2px solid #26c6f0;
  border-right: 2px solid #26c6f0;
  right: -1px;
  bottom: -1px;
}
</style>
`
}
