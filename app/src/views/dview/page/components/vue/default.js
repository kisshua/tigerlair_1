export default {
  code: `<template>
  <div
    :class="{'is-desktop-bg': value.isShowBg }"
    class="dog-desktop-panel"
  >
    <div v-if="value.isShowHead" class="desktop-panel-head">
      <div :title="value.name" class="head-title">
        <label>{{ value.name }}</label>
      </div>
    </div>
    <div class="desktop-panel-body">
      <div class="panel-content"></div>
    </div>
    <div class="border-foot"></div>
  </div>
</template>

<script>
const props = {
  name: 'Vue模板',
  isShowBg: true, // 显示背景
  isShowHead: true // 显示头
}
export default {
  props: {
    value: {
      type: Object,
      default() {
        return Object.assign({}, props)
      }
    }
  },
  
  data () {
	  return {}
  },

  created() {
    if (this.value.name === undefined) {
      Object.assign(this.value, props)
    }
  },

  mounted() {
    this.initView()
  },

  methods: {
    // 初始化视图
    initView() {}
  }
}
</script>

<style scoped>
.dog-desktop-panel {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  border: 1px solid #e0e0e0;
  border-radius: 4px;
  box-shadow: 0 2px 12px 0 rgb(0 0 0 / 10%);
}
.dog-desktop-panel .desktop-panel-head {
  padding: 0 16px;
  width: 100%;
}
.desktop-panel-head .head-title {
  height: 40px;
  font-size: 14px;
  color: #333;
  text-align: center;
  line-height: 40px;
  border-bottom: 1px solid #e0e0e0;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
}
.dog-desktop-panel .desktop-panel-body {
  height: 100%;
}
.desktop-panel-head + .desktop-panel-body {
  height: calc(100% - 40px);
}
.dog-desktop-panel.is-desktop-bg {
  background: #fff;
}
.desktop-panel-body .panel-content {
  padding: 8px 16px;
  color: #666;
}
</style>
`,
property: `<template>
  <div class="d-vue-property">
    Template Property
  </div>
</template>

<script>
export default {
  components: {},
  props: {
    value: {
      type: Object,
      default () {
        return {}
      }
    }
  },
  data () {
    return {}
  },
  methods: {}
}
</script>

<style>
.d-vue-property {
  padding: 8px;
  height: 100%;
  text-align: center;
  background: #fff;
  color: #666;
}
</style>
`
}
