export default {
  layout: {
    columns: 144,
    rows: 135,
    referHeight: 1080,
    margin: {
      top: '0px',
      right: '0px',
      bottom: '0px',
      left: '0px'
    },
    padding: {
      top: '8px',
      right: '8px',
      bottom: '8px',
      left: '8px'
    },
    contrast: {
      isAble: true,
      bgColor: '#ffffff'
    }
  },
  theme: {
    bgColor: '',
    bgImage: '',
    bgSize: '',
  }
}