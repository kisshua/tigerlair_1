import constant from '../absolute/constant'

export default {
  layout: {
    ...constant.layout,
    referWidth: 1920,
    contrast: {
      isAble: true,
      bgColor: '#000c3b'
    }
  }
}
