import constant from '../absolute/constant'

export default {
  layout: {
    ...constant.layout,
    rows: [{id: 1, rowHeight: 'auto'}, {id: 2, rowHeight: 'auto'}, {id: 3, rowHeight: 'auto'}],
    columns: [{id: 1, colWidth: 'auto'}, {id: 2, colWidth: 'auto'}, {id: 3, colWidth: 'auto'}],
    areas: [],
    rowGap: '8px',
    colGap: '8px'
  }
}
