# TigerLair


#### 演示地址
* <a href="http://www.daelui.com/#/tigerlair" target="_blank">http://www.daelui.com/#/tigerlair</a>


#### 介绍
- TigerLair(Tiger Lair) - 虎啸系统，无数据库，纯NodeJS环境
- 以前端的视角，实现项目管理工程化
- 前端页面在线开发，低代码拖拽式布局，自定义配置属性


#### 软件架构
- VUE + Express + DogUI + DogJS + OXJS


#### 安装教程
安装Node环境：http://nodejs.cn/


#### 使用说明
1.  命令行进入server目录
2.  执行命令：node app.js
3.  浏览器地址输入：http://127.0.0.1:xxxx/app/index.html (xxxx对应控制台输出的端口)