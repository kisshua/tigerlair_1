/**
 * @description 路由配置文件
*/

// 仅本地增、删、改
let permission = {
  // hosts: 'localhost',
  rules: ['admin']
}

/* 自定义控制器 */
const router = {
  routes: [
    // 导航
    {
      path: '/tigerlair/nave',
      controller: require('../modules/nave/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'href', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        },
        {
          path: 'list/treeall',
          method: 'get',
          action: 'queryTreeSub',
          excute: {
            sort: [
              {field: 'order', by: 'desc'}
            ],
            treeLevel: 18
          }
        },
        {
          path: 'list/tree',
          method: 'get',
          action: 'queryTreeSub',
          excute: {
            sort: [
              {field: 'order', by: 'desc'}
            ],
            treeLevel: 3
          }
        }
      ]
    },
    // 项目
    {
      name: 'project',
      path: '/tigerlair/project',
      controller: require('../modules/project/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    // 产品
    {
      name: 'product',
      path: '/tigerlair/product',
      controller: require('../modules/product/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        },
        {
          path: 'node/tree',
          method: 'get',
          action: 'queryNodeTree',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        }
      ]
    },
    // 页面
    {
      name: 'page',
      path: '/tigerlair/page',
      controller: require('../modules/page/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        },
        {
          path: 'list/treeall',
          method: 'get',
          action: 'queryTreeSub',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'productId', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ],
            treeLevel: 18
          }
        },
        {
          path: 'node/tree',
          method: 'get',
          action: 'queryNodeTree',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        }
      ]
    },
    {
      path: '/tigerlair/func', // 功能
      controller: require('../modules/func/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/codewell', // 编码库
      controller: require('../modules/codewell/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        },
        {
          path: 'node/tree',
          method: 'get',
          action: 'queryNodeTree',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        }
      ]
    },
    {
      name: 'archive',
      path: '/tigerlair/archive', // 文档
      controller: require('../modules/archive/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'cat', operator: 'IN'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'items', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'cat', operator: 'IN'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'items', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/section', // 章节
      controller: require('../modules/section/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'archiveId', operator: 'EQUAL'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/explore', // 文件浏览
      controller: require('../modules/explore/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'name', operator: 'LIKE'},
              {field: 'ext', operator: 'LIKE'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 2000},
              pageIndex: {field: 'pageIndex', default: 1}
            }
          },
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/arcfile', // 文档文件
      controller: require('../modules/arcfile/controller.js'),
      children: [
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'IN'},
              {field: 'aid', operator: 'IN'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      name: 'chapter',
      path: '/tigerlair/chapter', // 文档目录
      controller: require('../modules/chapter/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        },
        {
          path: 'node/tree',
          method: 'get',
          action: 'queryNodeTree',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'asc'},
              {field: 'order', by: 'desc'}
            ]
          }
        }
      ]
    },
    // 微应用
    {
      name: 'mapp',
      path: '/tigerlair/mapp',
      controller: require('../modules/mapp/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/viewpage', // 视图页面
      controller: require('../modules/viewpage/controller.js'),
      controllerName: 'viewpage',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'layout', operator: 'LIKE'},
              {field: 'mid', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            }
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'layout', operator: 'LIKE'},
              {field: 'mid', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/viewcomp', // 视图组件
      controller: require('../modules/viewcomp/controller.js'),
      controllerName: 'viewcomp',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'text', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'version', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            }
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'text', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'version', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/viewregister', // 组件注册表
      controller: require('../modules/viewregister/controller.js'),
      controllerName: 'viewregister',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'text', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'version', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'text', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'description', operator: 'LIKE'},
              {field: 'version', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        },
        {
          path: 'sync',
          method: 'post',
          action: 'syncEnvir',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/viewrelease', // 视图发布表
      controller: require('../modules/viewrelease/controller.js'),
      controllerName: 'viewrelease',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'vpid', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'vpid', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/snippet', // 片段
      controller: require('../modules/snippet/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      name: 'nav',
      path: '/tigerlair/nav', // 导航
      controller: require('../modules/nav/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'pid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        },
        {
          path: 'list/tree',
          method: 'get',
          action: 'queryTreeSub',
          excute: {
            sort: [
              {field: 'order', by: 'desc'}
            ],
            treeLevel: 16
          }
        }
      ]
    },
    {
      path: '/tigerlair/resource', // 数据源
      controller: require('../modules/resource/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            }
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        },
        {
          path: 'pool/:id',
          method: ['get', 'post'],
          action: 'pool'
        }
      ]
    },
    {
      path: '/tigerlair/pool', // 资源目录
      name: 'pool',
      controller: require('../modules/pool/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            }
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'path', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/dfile', // 文件
      controller: require('../modules/dfile/controller.js'),
      isPreInit: true,
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'originName', operator: 'LIKE'},
              {field: 'size', operator: 'LIKE'},
              {field: 'ext', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            fields: [
              {field: 'id'},
              {field: 'name'},
              {field: 'originName'},
              {field: 'size'},
              {field: 'ext'},
              {field: 'status'},
              {field: 'createTime'},
              {field: 'updateTime'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'originName', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        },
        {
          path: 'deploy',
          method: 'post',
          action: 'deploy',
          permission: permission
        },
        {
          path: 'upload',
          method: 'post',
          action: 'upload',
          middleware: 'uploadMiddleware',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/execute', // 处理流
      controllerName: 'execute',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/exeunit', // 处理单元
      controllerName: 'exeunit',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/exegroup', // 流程组
      controllerName: 'exegroup',
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'productId', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/inter', // 接口
      controller: require('../modules/inter/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN'},
              {field: 'method', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'eid', operator: 'IN'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN'},
              {field: 'method', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'eid', operator: 'IN'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/collect/inter', // 采集
      controller: require('../modules/collect-inter/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'gid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN', split: ','},
              {field: 'method', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'href', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'gid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN', split: ','},
              {field: 'method', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'href', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        // {
        //   path: 'item',
        //   method: 'put',
        //   action: 'add',
        //   permission: permission
        // },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'store',
          method: ['post', 'put'],
          action: 'store'
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/collect/inter/group', // 采集分组
      controller: require('../modules/collect-inter-group/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'IN'},
              {field: 'matchType', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'IN'},
              {field: 'matchType', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/collect/error', // 采集
      controller: require('../modules/collect-error/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'gid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'href', operator: 'LIKE'},
              {field: 'url', operator: 'LIKE'},
              {field: 'message', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'gid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN', split: ','},
              {field: 'method', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'href', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        // {
        //   path: 'item',
        //   method: 'put',
        //   action: 'add',
        //   permission: permission
        // },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'store',
          method: 'put',
          action: 'store'
        },
        {
          path: 'store',
          method: 'post',
          action: 'store'
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        },
        {
          path: 'sass',
          method: ['get', 'post', 'put', 'delete'],
          action: 'sass'
        }
      ]
    },
    {
      path: '/tigerlair/collect/error/group', // 采集分组
      controller: require('../modules/collect-error-group/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'IN'},
              {field: 'matchType', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'IN'},
              {field: 'matchType', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/collect/performance', // 采集
      controller: require('../modules/collect-performance/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'gid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'href', operator: 'LIKE'},
              {field: 'url', operator: 'LIKE'},
              {field: 'message', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'gid', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN', split: ','},
              {field: 'method', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'href', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'updateTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        // {
        //   path: 'item',
        //   method: 'put',
        //   action: 'add',
        //   permission: permission
        // },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'store',
          method: 'put',
          action: 'store'
        },
        {
          path: 'store',
          method: 'post',
          action: 'store'
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        },
        {
          path: 'sass',
          method: ['get', 'post', 'put', 'delete'],
          action: 'sass'
        }
      ]
    },
    {
      path: '/tigerlair/collect/performance/group', // 采集分组
      controller: require('../modules/collect-performance-group/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'IN'},
              {field: 'matchType', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'code', operator: 'LIKE'},
              {field: 'name', operator: 'IN'},
              {field: 'matchType', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/entity', // 实体
      controller: require('../modules/entity/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'entity', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'mid', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'entity', operator: 'LIKE'},
              {field: 'name', operator: 'LIKE'},
              {field: 'mid', operator: 'EQUAL'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/proxier', // 代理
      controller: require('../modules/proxier/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'status', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/plugins', // 插件
      controller: require('../modules/plugins/controller.js'),
      children: [
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'url', operator: 'LIKE'},
              {field: 'protocol', operator: 'IN'},
              {field: 'method', operator: 'LIKE'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'description', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'title', operator: 'LIKE'},
              {field: 'type', operator: 'EQUAL'},
              {field: 'labels', operator: 'LIKE'},
              {field: 'content', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/dictionary', // 字典
      controllerName: 'dictionary',
      tableName: 'dictionary',
      children: [
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'cat', operator: 'EQUAL'},
              {field: 'code', operator: 'EQUAL'},
              {field: 'key', operator: 'EQUAL'},
              {field: 'value', operator: 'LIKE'}
            ],
            sort: [
              {field: 'id', by: 'desc'},
              {field: 'cat', by: 'desc'},
              {field: 'code', by: 'desc'},
              {field: 'key', by: 'desc'},
              {field: 'value', by: 'desc'},
              {field: 'description', by: 'desc'},
              {field: 'remark', by: 'desc'},
              {field: 'dimension', by: 'desc'},
              {field: 'updateTime', by: 'desc'},
              {field: 'updateUser', by: 'desc'},
              {field: 'createTime', by: 'desc'},
              {field: 'createUser', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'list/page',
          method: 'get',
          action: 'queryAllByPage',
          excute: {
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'cat', operator: 'EQUAL'},
              {field: 'code', operator: 'EQUAL'},
              {field: 'key', operator: 'EQUAL'},
              {field: 'value', operator: 'LIKE'}
            ],
            limit: {
              pageSize: {field: 'pageSize', default: 10},
              pageIndex: {field: 'pageIndex', default: 1}
            },
            sort: [
              {field: 'id', by: 'desc'},
              {field: 'cat', by: 'desc'},
              {field: 'code', by: 'desc'},
              {field: 'key', by: 'desc'},
              {field: 'value', by: 'desc'},
              {field: 'description', by: 'desc'},
              {field: 'remark', by: 'desc'},
              {field: 'dimension', by: 'desc'},
              {field: 'updateTime', by: 'desc'},
              {field: 'updateUser', by: 'desc'},
              {field: 'createTime', by: 'desc'},
              {field: 'createUser', by: 'desc'},
              {field: 'order', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query',
          excute: {
            sort: [
              {field: 'id', by: 'desc'},
              {field: 'cat', by: 'desc'},
              {field: 'code', by: 'desc'},
              {field: 'key', by: 'desc'},
              {field: 'value', by: 'desc'},
              {field: 'description', by: 'desc'},
              {field: 'remark', by: 'desc'},
              {field: 'order', by: 'desc'},
              {field: 'dimension', by: 'desc'},
              {field: 'updateTime', by: 'desc'},
              {field: 'updateUser', by: 'desc'},
              {field: 'createTime', by: 'desc'},
              {field: 'createUser', by: 'desc'}
            ]
          }
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission: permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          excute: {
            sort: [
              {field: 'id', by: 'desc'}
            ]
          },
          permission: permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'delete',
          excute: {
            sort: [
              {field: 'id', by: 'desc'}
            ]
          },
          permission: permission
        },
        {
          path: 'list/tree',
          method: 'get',
          action: 'queryTreeSub',
          excute: {
            sort: [
              {field: 'order', by: 'desc'}
            ],
            treeLevel: 3
          }
        }
      ]
    },
    {
      path: '/tigerlair/user', // 用户页面
      controllerName: 'user',
      children: [
        {
          path: 'login',
          method: 'post',
          action: 'login'
        },
        {
          path: 'logout',
          method: 'post',
          action: 'logout'
        },
        {
          path: 'queryUserInfo',
          method: 'get',
          action: 'queryUserInfo'
        }
      ]
    },
    {
      path: '/tigerlair/actuator', // 执行器
      controller: require('../modules/actuator/controller.js'),
      children: [
        {
          path: 'execute',
          method: 'post',
          action: 'execute',
          excute: {},
          permission: permission
        }
      ]
    },
    {
      path: '/tigerlair/sass', // sass服务
      children: [
        // 采集接口响应
        {
          path: 'collect/inter',
          method: ['get', 'post', 'put', 'delete'],
          action: 'sass',
          controller: require('../modules/collect-inter/controller.js')
        },
        // 接口服务模拟
        {
          path: 'mock',
          method: ['get', 'post', 'put', 'delete'],
          action: 'sass',
          controller: require('../modules/inter/controller.js')
        }
      ]
    }
  ]
}

module.exports = router