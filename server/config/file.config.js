/**
 * @description 文件系统配置文件
*/

module.exports = {
  type: 'file', // 数据库类型
  hostname: '../data', // 服务器地址
  database: 'file', // 数据库名称
  username: '', // 用户名
  password: '', // 密码
  hostport: '', // 端口
  dsn: '', //
  params: '' // 数据库连接参数
}