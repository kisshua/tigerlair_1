/**
 * @description 服务端配置文件
*/

let constant = require('./constant.config')
let db = require('./db.config')
let file = require('./file.config')
let auth = require('./auth.config')

module.exports = {
  // 配置变量
  constant: constant,
  // 数据库配置
  db: db,
  // 文件库配置
  file: file,
  // 权限
  auth: auth
}