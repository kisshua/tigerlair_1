/*
 * @description 环境配置
 * @author: Rid King 
 * @since 2019-11-21 22:59:22 
 */

module.exports = {
  // 主机名
  hostname: '0.0.0.0',
	netPort: 3601,
	port: 3602,
  httpsPort: 3603,
  proxyPath: '/tigerlair/proxy',
  isProxyAble: true,
  isDynamicOrigin: true,
  isCheckToken: true,
  appPath: '/app',
  appDir: __dirname + '/../../app',
  poolPath: __dirname + '/../../../../daelui-kennel/pigsty/pool',
  uploadPath: __dirname + '/../../../../daelui-kennel/pigsty/pool/upload',
  proxierPath: __dirname + '/../../../../daelui-kennel/pigsty/df3s/tigerlair/app/extensions/proxier.js',
  pluginsPath: __dirname + '/../../../../daelui-kennel/pigsty/df3s/tigerlair/app/extensions/plugins.js',
	whiteList: [/\/(sass|app|pool|login|logout|queryUserInfo|dogui\/docs)(?=(\s*$|\/))|favicon\.ico$|\/index.html$|\/static\//]
}