/**
 * @description 文件浏览器-服务
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')
const Model = require('./model.js')
const { filer } = require('@daelui/oxkit')

class Service extends ox.Service {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 模型
    this.$model = new Model(args)
  }
  /**
   * @function 查询所有数据
   * @param {Object} action 操作集合
   * @demo
   * {
   *  req: {Request}
   *  res: {Response}
   *  data: {Object}
   * }
   * @return {Array}
  */
  async queryAll (action) {
    let result = []
    let params = action.params || {}
    let path = params.path
    let isFindAll = params.isFindAll
    let isIncludeDir = params.isIncludeDir
    let name = params.name
    let ext = params.ext
    let ignoreDir = params.ignoreDir
    let ignoreFile = params.ignoreFile
    let ignoreExt = params.ignoreExt
    if (!path) {
      let list = await filer.readOsLetters()
      result = {
        data: list,
        pager: {
          pageIndex: 1,
          pageSize: 1000,
          total: list.length    
        }
      }
    } else {
      // 目录数据
      action.excute = action.excute || {}
      action.excute.dataSource = {
        url: path,
        table: '',
        isFindSub: true,
        isFindAll: false,
        isIncludeDir: true,
        name,
        ext,
        ignoreDir,
        ignoreFile,
        ignoreExt
      }
      result = await this.$model.find(action)
    }

    return result
  }
}

module.exports = Service