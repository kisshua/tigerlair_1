/**
 * @description 文件浏览器-模型
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')

class Model extends ox.Model {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 表配置
    let file = this.route.scope.CONFIG.file
    let DBClass = ox.dbFactory.getDBClass(file.type)
    // 数据库
    this.$db = new DBClass(file)
    // 表名
    this.table = 'explore'
    // 字段
    this.fields = {
      id: {}
    }
  }
}

module.exports = Model