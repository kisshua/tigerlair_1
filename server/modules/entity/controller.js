/**
 * @description 实体-控制器
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')
const Service = require('./service.js')

class Controller extends ox.Controller {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 服务
    this.$service = new Service(args)
  }

  /**
   * @function 增删改事件触发
  */
  emit (name, action, res) {
    super.emit(name, action, res)
    res = res || {}
    if (res.success && /add|update/.test(name)) {
      this.syncInters(name, res, action)
    }
  }

  /**
   * @function 同步接口数据
  */
  async syncInters (name, res, action) {
    let params = action.params
    let eid = -1
    if (name === 'add') {
      eid = res.data
    } else if (name === 'update') {
      eid = params.id
    }
    // 查询所有的分组数据
    let result = await this.$router.getControllInstance({path: '/tigerlair/inter'}).queryAll({eid: eid}, {
      action: {},
      query: {},
      params: {},
      excute: {
        operators: [],
        params: {
          eid: params.eid
        },
        excute: {
          operators: [
            { field: 'eid', operator: 'EQUAL' }
          ],
          sort: [
            {field: 'createTime', by: 'desc'},
            {field: 'order', by: 'desc'}
          ]
        }
      }
    })
    let list = result.data
    list = Array.isArray(list) ? list : []
  }

  getInters (entity) {
    let entiy = entity.entity || 'api'
    let node = {
      eid: entity.id,
      'interface': '',
      'url': '',
      'protocol': 'http,https',
      'method': 'post',
      'description': '',
      'explain': '',
      'status': 1,
      'isBuildService': 1,
      'developer': '',
      'labels': '',
      'updateTime': 1671612957777,
      'updateUser': '',
      'createTime': 1671515911786,
      'createUser': '',
      'request': '{}',
      'reqHeaders': '[]',
      'reqType': '',
      'response': entiy.fields,
      'resHeaders': '[]',
      'resType': '',
      'resStatus': '',
      'reqStart': '',
      'reqEnd': '',
      'resTime': '',
      'env': '',
      'scene': '',
      'server': '',
      'isCache': '',
      'cacheExpire': '',
      'cacheType': '',
      'isBuffer': '',
      'bufferExpire': '',
      'isPreloader': '',
      'gateway': '',
      'isReqXSS': 1,
      'reqXSSField': '',
      'isResXSS': 1,
      'resXSSField': '',
      'gid': '',
      'mid': '',
      'order': '',
      'sign': '',
      'isStatic': 0,
      'static': ''
    }
    // 接口模板
    let interTplList = [
      { id: 'queryItem', text: 'queryItem', method: 'get' },
      { id: 'queryListByPage', text: 'queryListByPage', method: 'get' },
      { id: 'queryListAll', text: 'queryListAll', method: 'get' },
      { id: 'queryTreeAll', text: 'queryTreeAll', method: 'get' },
      { id: 'insert', text: 'insert', method: 'put' },
      { id: 'insertAll', text: 'insertAll', method: 'put' },
      { id: 'update', text: 'update', method: 'post' },
      { id: 'updateAll', text: 'updateAll', method: 'post' },
      { id: 'delete', text: 'delete', method: 'delete' },
      { id: 'deleteAll', text: 'deleteAll', method: 'delete' }
    ]
    let inters = interTplList.map(item => {
      let inter = { ...node }
      inter.interface = item.id
      inter.name = item.id
      inter.url = `{host}/sass/service/${entiy}/${item.id}`
      inter.method = item.method
    })
    return inters
  }
}

module.exports = Controller