/**
 * @description 数据键值对-模型
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')

class NaveModel extends ox.Model {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 表配置
    let db = this.route.scope.CONFIG.db
    // 表名
    this.table = 'nave'
    // 指定DB类
    let DBClass = ox.dbFactory.getDBClass(db.type)
    // 数据库
    this.$db = new DBClass(db)
    // 字段
    this.fields = {
      id: {}
    }
  }

  /**
   * @function 净化数据
   * @param {Object} data
   * @param {Object} config 更新类型
   * @return {Object}
   */
  pure (data, config) {
    config = config || {}
    // // 插入数据
    // if (config.isInsert) {

    // }
    // // 更新数据
    // else if (config.isUpdate) {

    // }

    return data
  }
}

module.exports = NaveModel