/**
 * @description 数据键值对-服务
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')
const NaveModel = require('./model.js')

const Service = ox.Service

class NaveService extends Service {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 模型
    this.$model = new NaveModel(args)
  }
}

module.exports = NaveService