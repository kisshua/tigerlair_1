/**
 * @description 文档-控制器
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')
const Service = require('./service.js')

class Controller extends ox.Controller {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 服务
    this.$service = new Service(args)
  }

  /**
   * @function 查询产品下的所有页面与功能
  */
  queryNodeTree (action) {
    // 解析数据
    return this.query(action).then(async res => {
      let chapter = res.data || {}
      chapter.children = Array.isArray(chapter.children) ? chapter.children : []
      if (chapter && chapter.id) {
        // 查询所有的页面数据
        let pages = await this.$router.getControllInstance({name: 'archive'}).queryTreeSub({}, {
          action: {},
          query: {},
          params: {
            id: '',
            chapterId: chapter.id
          },
          excute: {
            operators: [
              {field: 'chapterId', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'asc'},
              {field: 'order', by: 'desc'}
            ],
            treeLevel: 99
          }
        })
        pages = (pages.data || {}).list
        pages = Array.isArray(pages) ? pages : []
        chapter.children = pages
      }
      return {data: {list: [chapter]}, success: 1}
    })
  }
}

module.exports = Controller