/**
 * @description 文档-控制器
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')
const Service = require('./service.js')

class Controller extends ox.Controller {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 服务
    this.$service = new Service(args)
  }

  /**
   * @function 查询编码库下的所有文档
  */
   queryNodeTree (action) {
    // 解析数据
    return this.solveAction(action).then(async action => {
      let params = action.params || {}
      let pages = []
      if (params.id) {
        // 查询所有的文档数据
        pages = await this.$router.getControllInstance({name: 'archive'}).queryTreeSub({}, {
          action: {},
          query: {},
          params: {
            cid: params.id,
            cat: 1
          },
          excute: {
            operators: [
              {field: 'cid', operator: 'EQUAL'},
              {field: 'cat', operator: 'EQUAL'}
            ],
            sort: [
              {field: 'createTime', by: 'desc'},
              {field: 'order', by: 'desc'}
            ],
            treeLevel: 99
          }
        })
        pages = (pages.data || {}).list
        pages = Array.isArray(pages) ? pages : []
      }
      return {data: {list: pages}, success: 1}
    })
  }
}

module.exports = Controller